package AppPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Cristian on 8/3/2017.
 */
public class GuruPage {

    WebDriver driver;

    public GuruPage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(id = "vfb-6-0")
    private WebElement checkbox1;

    @FindBy(id = "vfb-6-1")
    WebElement checkbox2;

    @FindBy(id = "vfb-6-0")
    WebElement checkbox3;

    public void turnOnOff(){
        checkbox1.click();

        /*if(checkbox1.isSelected()){
            System.out.println("Checkbox is toggled on");
        } else {
            System.out.println("Checkbox is toggled off");
        }

        checkbox1.click();

        if(!checkbox1.isSelected()){
            System.out.println("Checkbox is now Togglet Off");
        }*/
    }

}
