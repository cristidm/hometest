package AppPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Cristian on 4/22/2017.
 */
public class LoginPage {

    WebDriver driver;

    //WebElements
    By userField = By.name("uid");
    By passField = By.name("password");
    By loginButton = By.name("btnLogin");
    By pageTitle = By.className("barone");

    //Constructor
    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    //Methods
    public void LoginMethod(String user,String pass){
        driver.findElement(userField).sendKeys(user);
        driver.findElement(passField).sendKeys(pass);
        driver.findElement(loginButton).click();
    }

    public String getPageTitle(){
        return driver.findElement(pageTitle).getText();
    }
}
