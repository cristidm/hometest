package AppPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by Cristian on 4/22/2017.
 */
public class HomePage {

    WebDriver driver;

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(className = "heading3")
    WebElement welcome;

    @FindBy(className = "barone")
    WebElement titlePage;

    @FindBy(xpath = "//a[contains(.,'New Customer')]")
    WebElement newCustomer;

    public void theWelcome(){
        System.out.println(welcome.getText());
        System.out.println(titlePage.getText());
    }

    public void createCustomer(){
//        WebElement newCustomer = driver.findElement(By.cssSelector(".menusubnav"));
//        List<WebElement> managerList = newCustomer.findElements(By.tagName("li"));
//        for (WebElement li : managerList) {
//            if (li.getText().equals("New Customer")) {
//                li.click();
//            }
//        }
        newCustomer.click();
    }

}
