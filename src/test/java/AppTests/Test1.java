package AppTests;

import AppPages.HomePage;
import AppPages.LoginPage;
import com.gargoylesoftware.htmlunit.Page;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by Cristian on 4/22/2017.
 */
public class Test1 {

    WebDriver driver;
    LoginPage loginPage;
    HomePage homePage;


    @BeforeClass
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","C:\\Users\\Cristian\\Downloads\\geckodriver2\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/V5/");
        homePage = PageFactory.initElements(driver, HomePage.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }

    @Test(priority = 1)
    public void myFirstTest(){
        loginPage.LoginMethod("mngr75512","hEpUpyn");
        String loginTitle = loginPage.getPageTitle();
        System.out.println("The title is: " + loginTitle);
        Assert.assertEquals(loginTitle, "Guru99 Bank");
    }

    @Test(priority = 2)  // (priority = 2, enabled = false) --> face skip la test
    public void myHomePageTest() throws InterruptedException {
        homePage.theWelcome();
        Thread.sleep(3000);
        homePage.createCustomer();

    }
}
