package AppTests;

import AppPages.GuruPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by Cristian on 8/3/2017.
 */
public class GuruTest {

    WebDriver driver;
    String URL = "http://demo.guru99.com/selenium/webform/radio.html";
    GuruPage myGuruPage;

    @BeforeTest
    public void Setup(){
        System.setProperty("webdriver.gecko.driver","C:\\Users\\Cristian\\Downloads\\geckodriver2\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(URL);
    }

    @AfterTest
    public void Close(){
        driver.quit();
    }

    @Test
    public void CheckboxTest(){

        myGuruPage.turnOnOff();
        System.out.println("Test completed with success!");

    }
}
