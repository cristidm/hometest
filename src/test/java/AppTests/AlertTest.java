package AppTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by Cristian on 7/27/2017.
 */
public class AlertTest {

    WebDriver driver;

    String baseURL = "http://output.jsbin.com/usidix/1";
    String alertMessage = "";


    @BeforeTest
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","C:\\Users\\Cristian\\Downloads\\geckodriver2\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void AlertMethod() {

        //Instance of WebDriverWait
        WebDriverWait myWaitVar = new WebDriverWait(driver, 10);

        //The wait condition for the element - myWaitVar
        myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[value=\"Go!\"]")));

        //Click on the element
        driver.findElement(By.cssSelector("input[value=\"Go!\"]")).click();

        //When the alert is present then click on it
        if(myWaitVar.until(ExpectedConditions.alertIsPresent()) != null){
            System.out.println("Alert is present!!!");
        }

        //Get the text from the alert
        alertMessage = driver.switchTo().alert().getText();

        //Accept the alert (click in ok)
        driver.switchTo().alert().accept();

        //Show the following message + the message from alert
        System.out.println("The message from Alert Popup is: " + alertMessage);
    }


}
