package AppTests;

import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by Cristian on 7/27/2017.
 */
public class SimpleTest {

    WebDriver driver;

    String baseURL = "http://demo.guru99.com/selenium/newtours/";
    String expectedTitle = "Welcome: Mercury Tours";
    String actualTitle = "";

    @BeforeTest
    public void SetUp(){
        System.setProperty("webdriver.gecko.driver","C:\\Users\\Cristian\\Downloads\\geckodriver2\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(baseURL);
    }

    @AfterTest
    public void TearDown(){
        driver.close();
        driver.quit();
    }

    @Test
    public void SimpleTest(){
        actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle,expectedTitle);
        System.out.println(">> Success! The actual title is the same like the expected title! <<");
    }

}
